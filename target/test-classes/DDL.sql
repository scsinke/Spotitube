CREATE TABLE users (
  user     VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL,
  token    CHAR(14)     NULL,
  PRIMARY KEY (user),
  UNIQUE (token)
);

CREATE TABLE playlists (
  id   MEDIUMINT    NOT NULL AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  user VARCHAR(255) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (user) REFERENCES users (user)
    ON DELETE CASCADE
);

CREATE TABLE tracks (
  id              MEDIUMINT NOT NULL AUTO_INCREMENT,
  title           VARCHAR(255),
  performer       VARCHAR(255),
  duration        INT(11)   NOT NULL,
  album           VARCHAR(255),
  playcount       INT(11),
  publicationDate DATE,
  description     TEXT,
  PRIMARY KEY (id)
);

CREATE TABLE tracksInPlaylist (
  playlistId MEDIUMINT  NOT NULL,
  trackId    MEDIUMINT  NOT NULL,
  offline    TINYINT(1) NOT NULL,
  PRIMARY KEY (playlistId, trackId),
  FOREIGN KEY (playlistId) REFERENCES playlists (id)
    ON DELETE CASCADE,
  FOREIGN KEY (trackId) REFERENCES tracks (id)
    ON DELETE CASCADE

);

CREATE VIEW playlistDuration
  AS
    SELECT
      playlists.id AS playlistId,
      sum(tracks.duration) AS duration
    FROM playlists, tracks, tracksInPlaylist
    WHERE playlists.id = tracksInPlaylist.playlistId AND tracksInPlaylist.trackId = tracks.id
    GROUP BY playlists.id