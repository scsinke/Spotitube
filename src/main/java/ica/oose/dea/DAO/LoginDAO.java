package ica.oose.dea.DAO;

import ica.oose.dea.DAO.util.DatabaseConnection;
import ica.oose.dea.POJO.User;

import javax.security.auth.login.CredentialException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginDAO extends DatabaseConnection implements ILoginDAO {

    public User getUserByUsername(String username) throws SQLException {
        User user = new User();
        openConnection();

        statement = connection.prepareStatement(queries.getUserByUsername());
        statement.setString(1, username);

        ResultSet resultSet = statement.executeQuery();
        resultSet.next();

        user.setUser(
                resultSet.getString("user"),
                resultSet.getString("password"),
                resultSet.getString("token")
        );

        closeConnection();
        return user;
    }

    public User getUserByToken(String token) throws SQLException {
        User user = new User();
        openConnection();

        statement = connection.prepareStatement(queries.getUserByToken());
        statement.setString(1, token);

        ResultSet resultSet = statement.executeQuery();
        resultSet.first();

        user.setUser(
                resultSet.getString("user"),
                resultSet.getString("password"),
                resultSet.getString("token")
        );

        closeConnection();
        return user;
    }

    public boolean verifyUser(User user, String token) throws CredentialException {
        if (user.getUsername() != null && user.getToken().equals(token)) {
            return true;
        } else {
            throw new CredentialException();
        }
    }

    public int setToken(String username, String token) throws SQLException {
        openConnection();

        statement = connection.prepareStatement(queries.setToken());
        statement.setString(1, token);
        statement.setString(2, username);

        int numberOffRowsEffected = statement.executeUpdate();

        closeConnection();
        return numberOffRowsEffected;
    }


}
