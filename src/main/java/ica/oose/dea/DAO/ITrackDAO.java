package ica.oose.dea.DAO;

import ica.oose.dea.POJO.Track;

import java.sql.SQLException;
import java.util.List;

public interface ITrackDAO {
    List<Track> getTracksNotInPlaylist(Integer playlistId) throws SQLException;

    boolean offlineAvailable(Integer trackId, Integer playlistId) throws SQLException;

    int addTrackToPlaylist(Integer trackId, Integer playlistId, Boolean offline) throws SQLException;

    List<Track> getTracksInPlaylist(Integer playlistId) throws SQLException;

    int deleteTrackFromPlaylist(Integer playlistId, Integer trackId) throws SQLException;
}
