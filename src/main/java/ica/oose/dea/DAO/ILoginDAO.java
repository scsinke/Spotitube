package ica.oose.dea.DAO;

import ica.oose.dea.POJO.User;

import javax.security.auth.login.CredentialException;
import java.sql.SQLException;

public interface ILoginDAO {
     User getUserByUsername(String username) throws SQLException;
     User getUserByToken(String token) throws SQLException;

    boolean verifyUser(User user, String token) throws CredentialException;

    int setToken(String username, String token) throws SQLException;
}
