package ica.oose.dea.DAO;

import ica.oose.dea.DAO.util.DatabaseConnection;
import ica.oose.dea.POJO.Playlist;
import ica.oose.dea.POJO.PlaylistResponse;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PlaylistDAO extends DatabaseConnection implements IPlaylistDAO {

    public int addPlaylist(String name, String username) throws SQLException {
        openConnection();
        statement = connection.prepareStatement(queries.addPlaylist());
        statement.setString(1, name);
        statement.setString(2, username);

        int numberOfRowsEffected = statement.executeUpdate();
        closeConnection();
        return numberOfRowsEffected;
    }

    public List<PlaylistResponse> getAllPlaylists(String username) throws SQLException {
        List<Playlist> playlists = new ArrayList<Playlist>();
        List<PlaylistResponse> playlistsResponse = new ArrayList<PlaylistResponse>();

        openConnection();

        statement = connection.prepareStatement(queries.getAllPlaylists());
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            Playlist playlist = new Playlist();

            playlist.setPlaylist(
                    resultSet.getInt("id"),
                    resultSet.getString("name"),
                    resultSet.getString("user")
            );

            playlists.add(playlist);
        }

        closeConnection();

        for (Playlist playlist: playlists) {
            PlaylistResponse playlistResponse = new PlaylistResponse();
            playlistResponse.setId(playlist.getId());
            playlistResponse.setName(playlist.getName());
            if (username.equals(playlist.getUser())){
                playlistResponse.setOwner(true);
            } else {
                playlistResponse.setOwner(false);
            }
            playlistsResponse.add(playlistResponse);

        }
        return playlistsResponse;
    }

    public Playlist getPlaylistById(Integer id) throws SQLException {
        List<Playlist> playlists = new ArrayList<Playlist>();
        openConnection();

        statement = connection.prepareStatement(queries.getAllPlaylists());
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            Playlist playlist = new Playlist();

            playlist.setPlaylist(
                    resultSet.getInt("id"),
                    resultSet.getString("name"),
                    resultSet.getString("user")
            );

            playlists.add(playlist);
        }

        closeConnection();

        for (Playlist playlist: playlists) {
            if (playlist.getId().equals(id)){
                return playlist;
            }
        }

        return null;
    }

    public int editPlaylist(Integer playlistId, String name) throws SQLException {
        openConnection();
        statement = connection.prepareStatement(queries.editPlaylist());
        statement.setString(1,name);
        statement.setInt(2,playlistId);

        int numberOfRowsEffected = statement.executeUpdate();

        closeConnection();
        return numberOfRowsEffected;
    }

    public int deletePlaylist(Integer id) throws SQLException {
        openConnection();
        statement = connection.prepareStatement(queries.deletePlaylist());
        statement.setInt(1,id);

        int numberOfRowsEffected = statement.executeUpdate();

        closeConnection();
        return numberOfRowsEffected;
    }

    public int getPlaylistLength(Integer playlistId)  {
        int length = 0;
        openConnection();

        try {
            statement = connection.prepareStatement(queries.getPlaylistLength());
            statement.setInt(1,playlistId);

            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                length = resultSet.getInt("duration");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        closeConnection();
        return length;
    }
}
