package ica.oose.dea.DAO;

import ica.oose.dea.DAO.util.DatabaseConnection;
import ica.oose.dea.POJO.Track;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TrackDAO extends DatabaseConnection implements ITrackDAO {
    public List<Track> getTracksNotInPlaylist(Integer playlistId) throws SQLException {

        List<Track> tracks = new ArrayList<Track>();
        openConnection();

        statement = connection.prepareStatement(queries.getTracksNotInPlaylist());
        statement.setInt(1, playlistId);

        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            Track track = new Track();

            track.setTrack(
                    resultSet.getInt("id"),
                    resultSet.getInt("duration"),
                    resultSet.getInt("playcount"),
                    resultSet.getString("title"),
                    resultSet.getString("performer"),
                    resultSet.getString("album"),
                    resultSet.getString("description"),
                    resultSet.getString("publicationDate")
            );

            tracks.add(track);
        }
        closeConnection();
        return tracks;
    }

    public boolean offlineAvailable(Integer trackId, Integer playlistId) throws SQLException {
        boolean trackOffline = false;
        openConnection();

        statement = connection.prepareStatement(queries.offlineAvailable());
        statement.setInt(1, trackId);
        statement.setInt(2, playlistId);

        ResultSet resultSet = statement.executeQuery();
        resultSet.first();

        if (resultSet.getBoolean("offline")) {
            trackOffline = true;
        }

        closeConnection();
        return trackOffline;
    }

    public int addTrackToPlaylist(Integer trackId, Integer playlistId, Boolean offline) throws SQLException {
        openConnection();

        statement = connection.prepareStatement(queries.addTrackToPlaylist());
        statement.setInt(1, trackId);
        statement.setInt(2, playlistId);
        statement.setBoolean(3, offline);

        int numberOffRowsEffected = statement.executeUpdate();
        closeConnection();
        return numberOffRowsEffected;
    }

    public List<Track> getTracksInPlaylist(Integer playlistId) throws SQLException {
        List<Track> tracks = new ArrayList<Track>();
        openConnection();
        statement = connection.prepareStatement(queries.getTracksInPlaylist());
        statement.setInt(1, playlistId);

        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            Track track = new Track();

            track.setTrack(
                    resultSet.getInt("id"),
                    resultSet.getInt("duration"),
                    resultSet.getInt("playcount"),
                    resultSet.getString("title"),
                    resultSet.getString("performer"),
                    resultSet.getString("album"),
                    resultSet.getString("description"),
                    resultSet.getString("publicationDate")
            );

            tracks.add(track);
        }
        closeConnection();
        return tracks;
    }

    public int deleteTrackFromPlaylist(Integer playlistId, Integer trackId) throws SQLException {
        openConnection();

        statement = connection.prepareStatement(queries.deleteTrackFromPlaylist());
        statement.setInt(1, playlistId);
        statement.setInt(2, trackId);

        int numberOffRowsEffected = statement.executeUpdate();
        closeConnection();
        return numberOffRowsEffected;
    }
}
