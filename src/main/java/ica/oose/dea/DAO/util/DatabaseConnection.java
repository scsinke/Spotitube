package ica.oose.dea.DAO.util;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseConnection {
    protected Logger logger = Logger.getLogger(getClass().getName());
    private Properties properties = new Properties();
    protected Connection connection;
    protected Queries queries = new Queries();

    protected PreparedStatement statement;

    protected DatabaseConnection(){
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("database.properties"));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "unable to load database properties", e);
        }
        loadDriver();
    }

    private void loadDriver(){
        try {
            Class.forName(properties.getProperty("driver"));
        } catch (ClassNotFoundException e) {
            logger.log(Level.SEVERE, "unable to load JDBC driver", e);
        }
    }

    protected void openConnection(){
        try {
            connection = DriverManager.getConnection(properties.getProperty("connection"));
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "unable to open connection");
        }
    }

    protected void closeConnection(){
        try {
            statement.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE,"unable to close statement", e);
        }

        try {
            connection.close();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "unable to close connection", e);
        }
    }

}
