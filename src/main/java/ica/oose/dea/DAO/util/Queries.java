package ica.oose.dea.DAO.util;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Queries {

    private Properties properties = new Properties();
    private Logger logger = Logger.getLogger(getClass().getName());

    public Queries() {
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("queriesMySQL.properties"));
        } catch (IOException e) {
            logger.log(Level.SEVERE, "unable to load queries", e);
        }
    }

    public String getUserByUsername() {
        return properties.getProperty("getUserByUsername");
    }

    public String getUserByToken() {
        return properties.getProperty("getUserByToken");
    }

    public String setToken() { return properties.getProperty("setToken"); }

    public String addPlaylist() { return properties.getProperty("addPlaylist"); }

    public String getAllPlaylists() { return properties.getProperty("getAllPlaylists"); }

    public String getPlaylistById() { return properties.getProperty("getPlaylistById"); }

    public String editPlaylist() { return  properties.getProperty("editPlaylist"); }

    public String deletePlaylist() { return properties.getProperty("deletePlaylist"); }

    public String getTracksNotInPlaylist() { return properties.getProperty("getTracksNotInPlaylist"); }

    public String offlineAvailable() { return properties.getProperty("offlineAvailable"); }

    public String addTrackToPlaylist() { return properties.getProperty("addTrackToPlaylist"); }

    public String getTracksInPlaylist() { return properties.getProperty("getTracksInPlaylist"); }

    public String deleteTrackFromPlaylist() { return properties.getProperty("deleteTrackFromPlaylist"); }

    public String getPlaylistLength() { return properties.getProperty("getPlaylistLength"); }
}
