package ica.oose.dea.DAO;

import ica.oose.dea.POJO.Playlist;
import ica.oose.dea.POJO.PlaylistResponse;

import java.sql.SQLException;
import java.util.List;

public interface IPlaylistDAO {

    int addPlaylist(String name, String username) throws SQLException;

    List<PlaylistResponse> getAllPlaylists(String username) throws SQLException;

    Playlist getPlaylistById(Integer id) throws SQLException;

    int editPlaylist(Integer playlistId, String name) throws SQLException;

    int deletePlaylist(Integer id) throws SQLException;

    int getPlaylistLength(Integer playlistId);
}
