package ica.oose.dea.Logic;

import ica.oose.dea.DAO.ILoginDAO;
import ica.oose.dea.DAO.IPlaylistDAO;
import ica.oose.dea.DAO.ITrackDAO;
import ica.oose.dea.POJO.Playlist;
import ica.oose.dea.POJO.Track;
import ica.oose.dea.POJO.User;

import javax.inject.Inject;
import javax.security.auth.login.CredentialException;
import java.sql.SQLException;
import java.util.List;

public class TrackLogic implements ITrackLogic {
    @Inject
    private ILoginDAO loginDAO;

    @Inject
    private ITrackDAO trackDAO;

    @Inject
    private IPlaylistDAO playlistDAO;

    public List<Track> getTracksNotInPlaylist(Integer playlistId, String token) throws SQLException, CredentialException {
        List<Track> tracks = null;
        User user = loginDAO.getUserByToken(token);

        if (loginDAO.verifyUser(user, token)) {
            tracks = trackDAO.getTracksNotInPlaylist(playlistId);
        }
        return tracks;
    }

    public void addTrackToPlaylist(Integer trackId, Integer playlistId, Boolean offline, String token) throws SQLException, CredentialException {
        User user = loginDAO.getUserByToken(token);
        Playlist playlist = playlistDAO.getPlaylistById(playlistId);

        if (loginDAO.verifyUser(user, token)) {
            if (isUserOwnerOfPlaylist(user.getUsername(), playlist.getUser())) {
                trackDAO.addTrackToPlaylist(trackId, playlistId, offline);
            }
        }
    }

    public List<Track> getTracksInPlaylist(Integer playlistId, String token) throws SQLException, CredentialException {
        List<Track> tracks = null;
        User user = loginDAO.getUserByToken(token);
        if (loginDAO.verifyUser(user, token)) {
            tracks = offlineAvailable(trackDAO.getTracksInPlaylist(playlistId), playlistId);
        }
        return tracks;
    }

    public void deleteTrackFromPlaylist(Integer playlistId, Integer trackId, String token) throws SQLException, CredentialException {
        User user = loginDAO.getUserByToken(token);
        Playlist playlist = playlistDAO.getPlaylistById(playlistId);

        if (loginDAO.verifyUser(user, token)) {
            if (isUserOwnerOfPlaylist(user.getUsername(), playlist.getUser())) {
                trackDAO.deleteTrackFromPlaylist(playlistId, trackId);
            }
        }
    }

    private List<Track> offlineAvailable(List<Track> tracks, Integer playlistId) throws SQLException {
        for (Track track : tracks) {
            if (trackDAO.offlineAvailable(track.getId(), playlistId)) {
                track.setOfflineAvailable(true);
            } else {
                track.setOfflineAvailable(false);
            }
        }
        return tracks;
    }

    private boolean isUserOwnerOfPlaylist(String username, String owner) {
        if (username.equals(owner)) {
            return true;
        }
        return false;
    }
}
