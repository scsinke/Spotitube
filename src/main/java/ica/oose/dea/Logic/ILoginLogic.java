package ica.oose.dea.Logic;

import ica.oose.dea.REST.Containers.UserResponse;

import javax.security.auth.login.CredentialException;
import java.sql.SQLException;

public interface ILoginLogic {
    UserResponse checkLoginCredentials(String username, String password) throws CredentialException, SQLException;
}
