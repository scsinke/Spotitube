package ica.oose.dea.Logic;

import ica.oose.dea.REST.Containers.PlaylistContainer;

import javax.security.auth.login.CredentialException;
import java.sql.SQLException;

public interface IPlaylistLogic {
    void addPlaylist(String token, Integer id, String name) throws CredentialException, SQLException;

    PlaylistContainer getAllPlaylists(String token) throws SQLException, CredentialException;

    void editPlaylist(Integer playlistId, String name, String token) throws SQLException, CredentialException;

    void deletePlaylist(Integer playlistId, String token) throws SQLException, CredentialException;
}
