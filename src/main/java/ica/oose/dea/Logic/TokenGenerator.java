package ica.oose.dea.Logic;

import java.util.Random;

public class TokenGenerator {
    public String generateToken() {
        String token = "";
        Random randomGenerator = new Random();

        for (int i = 0; i < 12; i++){

            if (i == 4 || i ==8) {
                token += "-";
            }
            token += randomGenerator.nextInt(9);
        }
        return token;
    }
}
