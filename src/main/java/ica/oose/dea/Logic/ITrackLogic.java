package ica.oose.dea.Logic;

import ica.oose.dea.POJO.Track;

import javax.security.auth.login.CredentialException;
import java.sql.SQLException;
import java.util.List;

public interface ITrackLogic {
    List<Track> getTracksNotInPlaylist(Integer playlistId, String token) throws SQLException, CredentialException;

    void addTrackToPlaylist(Integer trackId, Integer playlistId, Boolean offline, String token) throws SQLException, CredentialException;

    List<Track> getTracksInPlaylist(Integer playlistId, String token) throws SQLException, CredentialException;

    void deleteTrackFromPlaylist(Integer playlistId, Integer trackId, String token) throws SQLException, CredentialException;
}
