package ica.oose.dea.Logic;

import ica.oose.dea.DAO.ILoginDAO;
import ica.oose.dea.DAO.IPlaylistDAO;
import ica.oose.dea.POJO.Playlist;
import ica.oose.dea.POJO.PlaylistResponse;
import ica.oose.dea.REST.Containers.PlaylistContainer;
import ica.oose.dea.POJO.User;

import javax.inject.Inject;
import javax.security.auth.login.CredentialException;
import java.sql.SQLException;
import java.util.List;

public class PlaylistLogic implements IPlaylistLogic {

    @Inject
    private ILoginDAO loginDAO;
    @Inject
    private IPlaylistDAO playlistDAO;

    public void addPlaylist(String token, Integer id, String name) throws CredentialException, SQLException {
        User user = loginDAO.getUserByToken(token);

        if (loginDAO.verifyUser(user, token)) {
            playlistDAO.addPlaylist(name, user.getUsername());
        }

    }

    public PlaylistContainer getAllPlaylists(String token) throws SQLException, CredentialException {
        User user = loginDAO.getUserByToken(token);
        List<PlaylistResponse> playlistResponses = null;
        int length = 0;
        if (loginDAO.verifyUser(user, token)) {
            playlistResponses = playlistDAO.getAllPlaylists(user.getUsername());
        }
        for (PlaylistResponse playlist : playlistResponses) {
            length += playlistDAO.getPlaylistLength(playlist.getId());
        }

        PlaylistContainer playlistContainer = new PlaylistContainer(playlistResponses, length);
        return playlistContainer;
    }

    public void editPlaylist(Integer playlistId, String name, String token) throws SQLException, CredentialException {
        User user = loginDAO.getUserByToken(token);
        if (loginDAO.verifyUser(user, token)) {
            if (verifyUserIsOwnerOffPlaylist(playlistId, user)) {
                playlistDAO.editPlaylist(playlistId,name);
            }
        }

    }

    public void deletePlaylist(Integer playlistId, String token) throws SQLException, CredentialException {
        User user = loginDAO.getUserByToken(token);
        if (loginDAO.verifyUser(user, token)) {
            if (verifyUserIsOwnerOffPlaylist(playlistId, user)) {
                playlistDAO.deletePlaylist(playlistId);
            }
        }

    }

    private boolean verifyUserIsOwnerOffPlaylist(Integer playlistId, User user) throws CredentialException, SQLException {
        Playlist playlist = playlistDAO.getPlaylistById(playlistId);
        if (playlist.getUser().equals(user.getUsername())) {
            return true;
        } else {
            throw new CredentialException();
        }
    }
}
