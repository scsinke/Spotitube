package ica.oose.dea.Logic;

import ica.oose.dea.DAO.ILoginDAO;
import ica.oose.dea.POJO.User;
import ica.oose.dea.REST.Containers.UserResponse;

import javax.inject.Inject;
import javax.security.auth.login.CredentialException;
import java.sql.SQLException;

public class LoginLogic implements ILoginLogic {

    @Inject
    private ILoginDAO loginDAO;
    @Inject
    private TokenGenerator tokenGenerator;


    public UserResponse checkLoginCredentials(String username, String password) throws CredentialException, SQLException {
        User user = loginDAO.getUserByUsername(username);

        if (user.getUsername() != null && user.getPassword().equals(password)) {
            if (user.getToken() == null) {
                int numberOffRowsEffected = 0;
                while (numberOffRowsEffected <= 0){
                    numberOffRowsEffected = loginDAO.setToken(username, tokenGenerator.generateToken());
                }
                user = loginDAO.getUserByUsername(username);
            }

            UserResponse userResponse = new UserResponse(user.getUsername(), user.getToken());
            return userResponse;
        } else {
            throw new CredentialException();
        }
    }
}
