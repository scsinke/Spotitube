package ica.oose.dea.REST;

import ica.oose.dea.Logic.IPlaylistLogic;
import ica.oose.dea.Logic.ITrackLogic;
import ica.oose.dea.POJO.PlaylistResponse;
import ica.oose.dea.POJO.TrackToPlaylistRequest;
import ica.oose.dea.REST.Containers.TracksContainer;

import javax.inject.Inject;
import javax.security.auth.login.CredentialException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.sql.SQLException;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/playlists")
public class PlaylistService {

    @Inject
    IPlaylistLogic playlistLogic;
    @Inject
    ITrackLogic trackLogic;

    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @POST
    public Response addPlaylist(@QueryParam("token") String token, PlaylistResponse input) {
        try {
            playlistLogic.addPlaylist(token, input.getId(), input.getName());
            return Response.status(Response.Status.OK).entity(playlistLogic.getAllPlaylists(token)).build();
        } catch (SQLException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (CredentialException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }


    @Produces(APPLICATION_JSON)
    @GET
    public Response getAllPlaylists(@QueryParam("token") String token) {
        try {
            return Response.status(Response.Status.OK).entity(playlistLogic.getAllPlaylists(token)).build();
        } catch (SQLException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (CredentialException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }


    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @PUT
    @Path("/{playlistId}")
    public Response editPlaylist(@PathParam("playlistId") Integer playlistId, @QueryParam("token") String token, PlaylistResponse input) {
        try {
            playlistLogic.editPlaylist(playlistId, input.getName(), token);
            return Response.status(Response.Status.OK).entity(playlistLogic.getAllPlaylists(token)).build();
        } catch (SQLException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (CredentialException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @DELETE
    @Path("{playlistId}")
    public Response deletePlaylist(@PathParam("playlistId") Integer playlistId ,@QueryParam("token") String token) {
        try {
            playlistLogic.deletePlaylist(playlistId, token);
            return Response.status(Response.Status.OK).entity(playlistLogic.getAllPlaylists(token)).build();
        } catch (SQLException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (CredentialException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @POST
    @Path("{playlistId}/tracks")
    public Response addTrackToPlaylist(@PathParam("playlistId") Integer playlistId, @QueryParam("token") String token, TrackToPlaylistRequest input){
        try {
            trackLogic.addTrackToPlaylist(input.getId(), playlistId, input.isOfflineAvailable(), token);
            return Response.status(Response.Status.OK).entity(new TracksContainer(trackLogic.getTracksInPlaylist(playlistId, token))).build();
        } catch (SQLException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (CredentialException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @Produces(APPLICATION_JSON)
    @GET
    @Path("/{playlistId}/tracks")
    public Response getTracksInPlaylist(@PathParam("playlistId") Integer playlistId, @QueryParam("token") String token) {
        try {
            return Response.status(Response.Status.OK).entity(new TracksContainer(trackLogic.getTracksInPlaylist(playlistId, token))).build();
        } catch (SQLException e) {
            System.out.println(e);
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (CredentialException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

    @DELETE
    @Path("/{playlistId}/tracks/{trackId}")
    public Response deleteTrackFromPlaylist(@PathParam("playlistId") Integer playlistId, @PathParam("trackId") Integer trackId, @QueryParam("token") String token) {
        try {
            trackLogic.deleteTrackFromPlaylist(playlistId, trackId, token);
            return Response.status(Response.Status.OK).entity(new TracksContainer(trackLogic.getTracksInPlaylist(playlistId, token))).build();
        } catch (SQLException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (CredentialException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }

}
