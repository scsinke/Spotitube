package ica.oose.dea.REST;

import ica.oose.dea.Logic.ILoginLogic;
import ica.oose.dea.POJO.LoginRequest;
import ica.oose.dea.REST.Containers.UserResponse;

import javax.inject.Inject;
import javax.security.auth.login.CredentialException;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import java.sql.SQLException;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/login")
public class LoginService {

    @Inject
    private ILoginLogic loginLogic;


    @Consumes(APPLICATION_JSON)
    @Produces(APPLICATION_JSON)
    @POST
    public Response login(LoginRequest input){
        try {
            UserResponse userResponse = loginLogic.checkLoginCredentials(input.getUsername(), input.getPassword());
            return Response.status(Response.Status.OK).entity(userResponse).build();
        } catch (CredentialException e) {
            return  Response.status(Response.Status.UNAUTHORIZED).build();
        } catch (SQLException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }
}
