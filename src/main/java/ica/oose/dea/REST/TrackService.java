package ica.oose.dea.REST;

import ica.oose.dea.Logic.ITrackLogic;
import ica.oose.dea.REST.Containers.TracksContainer;

import javax.inject.Inject;
import javax.security.auth.login.CredentialException;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.sql.SQLException;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/tracks")
public class TrackService {

    @Inject
    ITrackLogic trackLogic;

    @Produces(APPLICATION_JSON)
    @GET
    public Response getTracksNotInPlaylist(@QueryParam("forPlaylist") Integer playlistId, @QueryParam("token") String token) {
        try {
            return Response.status(Response.Status.OK).entity(new TracksContainer(trackLogic.getTracksNotInPlaylist(playlistId,token))).build();
        } catch (SQLException e) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (CredentialException e) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }
    }
}
