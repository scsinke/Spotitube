package ica.oose.dea.REST.Containers;

public class UserResponse {
    private String token;
    private String user;

    public UserResponse(String username, String token) {
        this.user = username;
        this.token = token;
    }
}
