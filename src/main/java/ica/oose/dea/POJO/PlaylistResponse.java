package ica.oose.dea.POJO;

import java.util.ArrayList;
import java.util.List;

public class PlaylistResponse {
    private Integer id;
    private String name;
    private Boolean owner;
    private List<Track> tracks = new ArrayList<Track>();

    public void setPlaylistResponse(Integer id, String name, Boolean owner) {
        this.id = id;
        this.name = name;
        this.owner = owner;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getOwner() {
        return owner;
    }

    public void setOwner(Boolean owner) {
        this.owner = owner;
    }
}
