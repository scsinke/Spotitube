package ica.oose.dea.POJO;

import java.util.ArrayList;
import java.util.List;

public class Playlist {
    private Integer id;
    private String name;
    private String user;
    private List<Track> tracks = new ArrayList<Track>();

    public void setPlaylist(Integer id, String name, String user) {
        this.id = id;
        this.name = name;
        this.user = user;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUser() {
        return user;
    }
}
