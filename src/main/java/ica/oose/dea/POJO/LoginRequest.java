package ica.oose.dea.POJO;

public class LoginRequest {
    private String user;
    private String password;

    public String getUsername() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String user) {
        this.user = user;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
