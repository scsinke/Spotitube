package ica.oose.dea.POJO;

public class TrackToPlaylistRequest {
    private Integer id, duration;
    private String title, performer;
    private boolean offlineAvailable;

    public Integer getId() {
        return id;
    }

    public Integer getDuration() {
        return duration;
    }

    public String getTitle() {
        return title;
    }

    public String getPerformer() {
        return performer;
    }

    public boolean isOfflineAvailable() {
        return offlineAvailable;
    }
}
