package ica.oose.dea.POJO;

import java.sql.Date;

public class Track {
    private Integer id, duration, playcount;
    private String title, performer, album, description;
    private String publicationDate;
    private Boolean offlineAvailable;

    public void setTrack(Integer id, Integer duration, Integer playcount, String title, String performer, String album, String description, String publicationDate) {
        this.id = id;
        this.duration = duration;
        this.playcount = playcount;
        this.title = title;
        this.performer = performer;
        this.album = album;
        this.description = description;
        this.publicationDate = publicationDate;
    }

    public void setOfflineAvailable(Boolean offlineAvailable) {
        this.offlineAvailable = offlineAvailable;
    }

    public Integer getId() {
        return id;
    }
}
