
INSERT INTO users (user,password,token)
VALUES('Carsten','Test123','1234-1234-1234');

INSERT INTO users (user,password)
VALUES('Kurt', 'PASSWORD');

INSERT INTO playlists (name, user)
VALUES ('Pop', 'Carsten');

INSERT INTO playlists(name, user)
VALUES ('Mood Booster', 'Carsten');

INSERT INTO tracks (title,performer,duration,album,publicationDate)
VALUES ('Ocean and a rock', 'Lisa Hannigan', 337,'Sea sew','2014-09-03');

INSERT INTO tracks (title,performer,duration,album)
VALUES ('So long, Marianne', 'Leonard Cohen', 546,'Songs of Leonard Cohen');

INSERT INTO tracks (title,performer,duration,playcount,publicationDate,description)
VALUES ('One', 'Metallica', 423,37,'2012-01-11','Long version');

INSERT INTO tracksInPlaylist
VALUES(2, 3, TRUE);


