package ica.oose.dea.REST;

import ica.oose.dea.Logic.ILoginLogic;
import ica.oose.dea.POJO.LoginRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.security.auth.login.CredentialException;
import java.sql.SQLException;

import static org.mockito.Mockito.verify;

public class LoginTest {

    @Mock
    private ILoginLogic loginLogic;

    @InjectMocks
    private LoginService loginService;

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void TestLoginRest() throws CredentialException, SQLException {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("Carsten");
        loginRequest.setPassword("Test123");

        loginService.login(loginRequest);

        verify(loginLogic).checkLoginCredentials(loginRequest.getUsername(),loginRequest.getPassword());
    }
}
