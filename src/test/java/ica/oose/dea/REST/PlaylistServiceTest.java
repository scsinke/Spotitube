package ica.oose.dea.REST;

import ica.oose.dea.Logic.IPlaylistLogic;
import ica.oose.dea.POJO.Playlist;
import ica.oose.dea.POJO.PlaylistResponse;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.security.auth.login.CredentialException;

import java.sql.SQLException;

import static org.mockito.Mockito.verify;

public class PlaylistServiceTest {

    @Mock private IPlaylistLogic playlistLogic;
    @Rule public MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private PlaylistService playlistService;

    @Test
    public void TestPlaylistRest() throws CredentialException, SQLException {
        PlaylistResponse playlist = new PlaylistResponse();
        playlist.setPlaylistResponse(-1,"Mood booster",true);
        String token = "1234-1234-1234";
       playlistService.addPlaylist(token,playlist);

       verify(playlistLogic).addPlaylist(token,playlist.getId(),playlist.getName());

    }
}
