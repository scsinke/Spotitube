package ica.oose.dea.Logic;

import ica.oose.dea.DAO.ILoginDAO;
import ica.oose.dea.POJO.User;
import ica.oose.dea.REST.Containers.UserResponse;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.security.auth.login.CredentialException;

import java.sql.SQLException;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LoginLogicTest {

    @Mock
    private User user;
    @Mock
    private ILoginDAO loginDAO;
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private LoginLogic loginLogic;

    @Before
    public void setUp() throws Exception {
        //MockitoAnnotations.initMocks(this);
        when(loginDAO.getUserByUsername("Carsten")).thenReturn(user);
        when(user.getPassword()).thenReturn("PASSWORD");


    }

    @Test(expected = CredentialException.class)
    public void LoginLogicPasswordCheckFailed() throws CredentialException, SQLException {
        when(user.getUsername()).thenReturn("Carsten");
        //run method with wrong credentials
        loginLogic.checkLoginCredentials("Carsten", "Test123");

        //did loginlogic call getUser on DAO layer?
        verify(loginDAO).getUserByUsername("Carsten");
        verify(user).getPassword();
    }

    @Test(expected = CredentialException.class)
    public void loginLogicNotValidUser() throws CredentialException, SQLException {
        loginLogic.checkLoginCredentials("Carsten", "PASSWORD");

        verify(loginDAO).getUserByUsername("Carsten");
    }

}
