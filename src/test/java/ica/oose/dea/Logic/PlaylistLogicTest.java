package ica.oose.dea.Logic;

import ica.oose.dea.DAO.ILoginDAO;
import ica.oose.dea.DAO.IPlaylistDAO;
import ica.oose.dea.POJO.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.security.auth.login.CredentialException;

import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.isNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PlaylistLogicTest {

    @Mock
    private ILoginDAO loginDAO;
    @Mock
    private IPlaylistDAO playlistDAO;
    @Mock
    private User user;
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private PlaylistLogic playlistLogic;

    @Before
    public void setUp() throws SQLException {
        when(loginDAO.getUserByToken("1234-1234-1234")).thenReturn(user);
    }

    @Test
    public void PlaylistLogicCheckIfUserExist() throws CredentialException, SQLException {
        when(user.getUsername()).thenReturn("Carsten");
        when(user.getToken()).thenReturn("1234-1234-1234");

        playlistLogic.addPlaylist("1234-1234-1234", -1, "Mood booster");

        verify(loginDAO).getUserByToken("1234-1234-1234");
    }
}
