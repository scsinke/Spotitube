package ica.oose.dea.POJO;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PlaylistTest {
    @Test
    public void PlaylistTest(){
        PlaylistResponse playlist = new PlaylistResponse();
        playlist.setPlaylistResponse(10,"Mood booster",true);

        assertTrue(playlist.getId().equals(10));
        assertEquals("Mood booster", playlist.getName());
        assertTrue(playlist.getOwner().equals(true));

        PlaylistResponse playlist2 = new PlaylistResponse();
        playlist2.setId(20);
        playlist2.setName("After Summer");
        playlist2.setOwner(false);

        assertTrue(playlist2.getId().equals(20));
        assertEquals("After Summer", playlist2.getName());
        assertTrue(playlist2.getOwner().equals(false));

    }
}
