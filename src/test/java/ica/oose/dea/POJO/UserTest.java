package ica.oose.dea.POJO;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserTest {

    @Test
    public void UserPOJOTest(){
        User user = new User();
        user.setUser("Carsten", "Test123", "1234-1234-1234");
        assertEquals("Carsten",user.getUsername());
        assertEquals("Test123",user.getPassword());
        assertEquals("1234-1234-1234", user.getToken());

        User user2 = new User();
        user2.setUsername("Kurt");
        user2.setPassword("SuperSecret");
        user2.setToken("1111-1111-1111");
        assertEquals("Kurt",user2.getUsername());
        assertEquals("SuperSecret",user2.getPassword());
        assertEquals("1111-1111-1111", user2.getToken());
    }
}
