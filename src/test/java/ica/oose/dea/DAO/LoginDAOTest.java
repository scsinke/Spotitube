package ica.oose.dea.DAO;

import ica.oose.dea.POJO.User;
import org.junit.Test;

import java.sql.SQLException;

import static org.junit.Assert.*;

public class LoginDAOTest {

    private LoginDAO loginDAO = new LoginDAO();

    @Test
    public void getUserByUsername() throws SQLException {
        User user = loginDAO.getUserByUsername("Carsten");
        assertTrue(user.getUsername().equals("Carsten"));
        assertTrue(user.getPassword().equals("Test123"));
    }

    @Test(expected = SQLException.class)
    public void getUserByUsernameFailed() throws SQLException{
        loginDAO.getUserByUsername("Gerben");
    }

    @Test
    public void getUserByToken() throws SQLException {
        User user = loginDAO.getUserByToken("1234-1234-1234");
        assertTrue(user.getUsername().equals("Carsten"));
        assertTrue(user.getToken().equals("1234-1234-1234"));
    }




}